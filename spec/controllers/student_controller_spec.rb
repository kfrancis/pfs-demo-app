require 'rails_helper'

RSpec.describe StudentsController, :type => :controller do
  describe 'create student' do
    let(:new_student) { build(:student) }

    context 'with valid data' do
      let(:count) { Student.count }

      it 'should increase student count by 1' do
        subject{ post :create, student: attributes_for(:new_student) }
        expect{ subject.to change(Student, :count).by(1) }
      end
    end
  end
end
