FactoryGirl.define do
  factory :student do
    name "John Doe"
    age 21
    course "Computer Science"
  end
end